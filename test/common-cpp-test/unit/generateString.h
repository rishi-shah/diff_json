/*
 * @file generateString.h
 * @date 15 Feb 2019
 * @author Brett Lamb
 */

#ifndef common_tests_generateString
#define common_tests_generateString

//STL Includes
#include <random>

namespace common {
namespace tests {

static char generateChar()
{
    static const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    static const size_t maxIndex =
            sizeof(charset) - 1;

    std::random_device random;
    std::default_random_engine randomEngine(random());
    std::uniform_int_distribution<int> uniform_dist(0, maxIndex);
    return charset[uniform_dist(randomEngine) % maxIndex];
}

static void generateString(
        char* generatedString,
        const int size)
{
    for (int i=0; i<size; ++i)
    {
        generatedString[i] = generateChar();
    }
}

static std::string generateString(
        const int size)
{
    std::string generatedString;
    for (int i=0; i<size; ++i)
    {
        generatedString.push_back(
                generateChar());
    }
    return generatedString;
}

}; //namespace tests
}; //namespace common

#endif //common_tests_generateString
