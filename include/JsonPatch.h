/*
 * @file AlarmLogsTest.cpp
 * @date 1/13/2020
 * @author Rishi Shah
 */


#ifndef Json_Patch_tests_H_
#define Json_Patch_tests_H_

#include <gtest/gtest.h>
#include <iostream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <fstream>
#include <string>

using json = nlohmann::json;

namespace JsonDiffer {
namespace tests {

class JsonPatch
{

public:

	json mPatched;

	JsonPatch(const std::string& Source,const std::string& Target,
		std::vector <std::string> IgPath){

		readInit(Source, Target);

		mIgPath = IgPath;

	}

	JsonPatch(const std::string& Source,const std::string& Target){

		readInit(Source, Target);

	}

	void run(){

		//to ensure that the json is not empty or invalid
		if(toIgnJson(mSource) && toIgnJson(mTarget)){

			findreplace();

			//checking whether ignore paths exist
			if(toIgnPath()){
				fndIgPath();
			}
			else{
				mPatched = mPatch;		//result is the same if ignore paths don't exist
			}


			if(!yesDiff){

				std::cout<< "Messages Identical  \n" << std::endl;
				SUCCEED();

			}

			else {

				std::cout << "Messages not equal.  \n"
				<< "******Patch:*****\n"
				<< std::setw(4)<< mPatched << std::endl;
				SUCCEED();

			}

		}

		else{

			mPatched = {};							//A null Json is returned if invalid json or empty json

			std::cout << "Patch not generated.  \n"
					  <<"Json Parse Error"
					  << std::endl;

		}

	}


private:
	json mSource;
	json mTarget;
	json mPatch;
	bool sstream, tstream = false;
	bool yesDiff = false;
	std::vector <std::string> mIgPath;


	void readInit(const std::string& Source, const std::string& Target){

		std::ifstream sjs(Source);
		sstream = sjs.good();


		std::ifstream tjs(Target);
		tstream = tjs.good();

		if(tstream && sstream){
			try{
				sjs >> mSource;			//	will create exception if the json is invalid
				tjs >> mTarget;
			}
			catch (const std::exception& e)
			{
				mSource.clear();		//	empty json created in case of an invalid json or stream
				mTarget.clear();
				ASSERT_TRUE(true) << "ERROR: Exception in parse(): " << e.what() << std::endl;
			}
			catch (...)
			{
				mSource.clear();
				mTarget.clear();
				ASSERT_TRUE(true) << "ERROR: Exception in parse()" << std::endl;
			}

			if(mSource != mTarget){
				yesDiff = true;		// to flag identical or not
			}
		}

	}


	bool toIgnPath() { return !mIgPath.empty();}

	bool toIgnJson(json EmpJson) { return !EmpJson.empty();}

	void fndIgPath(){

		for(int i = 0; i < mPatch.size(); i++){

	    	json newobj = mPatch[i];
	    	std::string newpath = newobj["/path"_json_pointer];
	    	int dsexist = 0;

	    	if(toIgnPath()){
				for(int j = 0; j < mIgPath.size(); j++){
					int fnd = newpath.find(mIgPath[j]);
					if (newpath.length() - fnd == mIgPath[j].length()) {
						dsexist = 1;
					}
				}
	    	}
	    	if(!dsexist){
	    		mPatched.push_back(mPatch[i]);

	    	}
	    }

	}

	void findreplace(){

		json patch1 = json::diff(mSource, mTarget);
		json patch2 = json::diff(mTarget, mSource);

		int secit = 0;
		bool resultset = false;

		for(int i = 0; i < patch1.size(); i++){

			// looping through patch1 to find replace operations
	    	json newobj = patch1[i];
	    	std::string newop = newobj["/op"_json_pointer];

	    	if(newop != "replace"){
	    		mPatch.push_back(newobj);		// if operation is not replace, enter as is
	    	}

	    	else {

	    		std::string newpath1 = newobj["/path"_json_pointer]; 	// if operation is replace, path is found in patch2

	    		while(!resultset){

	    			json newobj2 = patch2[secit];
	    			std::string newpath2 = newobj2["/path"_json_pointer];

	    			if(newpath1 == newpath2){
	    				// replace operations appear in same order in both patches,
	    				// thus no need for searching the entire patch,
	    				// it can be done in order
	    				newobj ["new value"] = newobj2["/value"_json_pointer];
	    				mPatch.push_back(newobj);
	    	    		resultset = true;
	    			}

	    			secit++;

	    		}

	    		resultset = false;

	    	}

		}

	}


};
}
}
#endif
