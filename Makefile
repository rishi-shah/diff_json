USER=$(shell id -u):$(shell id -g)
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

CONTAINER_CODE_DIR=/opt/code

INIT_CMD=git submodule update --init --recursive

#Override when calling to do a "Release" build
BUILD_TYPE=Debug
BUILD_DIR=${CONTAINER_CODE_DIR}/build/${BUILD_TYPE}
BUILD_CMD=mkdir -p ${BUILD_DIR} && \
    cd ${BUILD_DIR} && \
    cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ../../ && \
    make all

TEST_CMD=export GTEST_COLOR=1 && cd ${BUILD_DIR}/test && ctest -V ${TEST_ARGS}

CLEAN_CMD=rm -rf ${BUILD_DIR}

DOCKER_BUILDER_IMAGE=alstommlb/builder-cpp:latest
DOCKER_PULL_CMD=docker pull ${DOCKER_BUILDER_IMAGE}
DOCKER_RUN_CMD=${DOCKER_PULL_CMD} && \
    docker run --rm \
        --user ${USER} \
        -v ${ROOT_DIR}:${CONTAINER_CODE_DIR} \
        -w ${CONTAINER_CODE_DIR} \
        ${DOCKER_BUILDER_IMAGE} /bin/bash -c

.PHONY: init build test analyze clean

#Having git installed on the host is a requirement.
#  Doing any git command that requires ssh is difficult in docker as user home directories, 
#  known_hosts, private keys, etc must be mapped in and configured.
init:
	${INIT_CMD}

build:
	${DOCKER_RUN_CMD} "${BUILD_CMD}"

build_local:
	${BUILD_CMD}

unit_test:
	${DOCKER_RUN_CMD} "${TEST_CMD}"

unit_test_local:
	${TEST_CMD}

test: unit_test

test_local: unit_test_local

analyze:

clean:
	${DOCKER_RUN_CMD} "${CLEAN_CMD}"

clean_local:
	${CLEAN_CMD}

#Everything right of the pipe is order-only prerequisites.
all: | init build test analyze

all_local: | init_local build_local test_local analyze_local
