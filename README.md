### Json Differ

## Development

Example steps for development via Eclipse.

1. git clone git@bitbucket.org:rishi-shah/diff_json.git
2. cd diff_json
3. git submodule update --init --recursive
4. cd {BUILD_DIR} *#BUILD\_DIR should not be inside of the source directory.*
5. mkdir diff_json\_DEBUG
6. cd diff_json\_DEBUG
7. cmake -DCMAKE_BUILD_TYPE=Debug -G "Eclipse CDT4 - Unix Makefiles" {SOUCE_DIR}
8. In eclipse, File-->Import-->General-->Existing Projects into Workspace.  Browse to {BUILD_DIR}/diff_json\_DEBUG.
