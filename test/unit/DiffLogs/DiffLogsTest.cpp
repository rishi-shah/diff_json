/*
 * @file DiffLogsTest.cpp
 * @date 1/13/2020
 * @author Rishi Shah
 */


#include <JsonPatch.h>
#include <gtest/gtest.h>
#include <iostream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <fstream>
#include <string>


using json = nlohmann::json;

namespace JsonDiffer{
namespace tests {

/**
// * Identical data
// */
TEST(DiffLogsTest, Identical)
{
	std::vector<std::string> ignorepath;

	ignorepath.push_back("time");
	ignorepath.push_back("date");

	JsonPatch idtest("Identical.json","Identical.json",ignorepath);

	idtest.run();
}

TEST(DiffLogsTest, Diff)
{
	std::vector<std::string> ignorepath;

	ignorepath.push_back("time");
	ignorepath.push_back("date");

	JsonPatch difftest("Valid.json","Valid2.json",ignorepath);

	difftest.run();
}



/*
 * Various lines don't exist.
 */
TEST(DiffLogsTest, Malformed)
{
	
	JsonPatch maltest("Valid.json","Malformed.json");

	maltest.run();
}
/*
 * No data
 */
TEST(DiffLogsTest, Empty)
{
	JsonPatch emptest("Valid.json","Empty.json");

	emptest.run();
}

/**
 * Unexpected lines encountered.
 */
TEST(DiffLogsTest, UnknownEntries)
{
	std::vector<std::string> ignorepath;

	ignorepath.push_back("pak raha hai");
	ignorepath.push_back("phlegm");

	JsonPatch uetest("Valid.json","Valid2.json",ignorepath);

	uetest.run();
}




}; //namespace tests
}; //namespace DiffLogs
