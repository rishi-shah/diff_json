//
////Protobuf includes
//#ifndef UNIT_TEST_NO_PROTO
//#include <google/protobuf/stubs/common.h>
//#endif

//gtest Includes
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    const int retVal = RUN_ALL_TESTS();

//If your unit test executable doesn't use protobufs, disable this call in the test's CMakeLists.txt:
//  add_definitions(-DUNIT_TEST_NO_PROTO)
//#ifndef UNIT_TEST_NO_PROTO
//    //Not necessary but prevents memory leak reporting in analysis tools.
//    //  Leave enabled if possible.
//    google::protobuf::ShutdownProtobufLibrary();
//#endif

    return retVal;
}

