//gtest Includes

#ifndef Json_Patch_tests_H_
#define Json_Patch_tests_H_

#include <gtest/gtest.h>
#include <iostream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <fstream>
#include <string>

using json = nlohmann::json;

namespace JsonDiffer {
namespace tests {

class JsonPatch
{

public:

	JsonPatch(json Source, json Target,
		std::vector <std::string> IgPath){

		setInit(Source, Target, IgPath);


	}

	void run(){

		try
		{
			fndIgPath();
		}
		catch(const std::exception& e)
		{
			ASSERT_TRUE(false) << "ERROR: Exception in parse()" << e.what() << std::endl;
		}
		catch (...)
		{
		    ASSERT_TRUE(false) << "ERROR: Exception in parse()" << std::endl;
		}


		if(yesDiff){
			FAIL()
					   << "Messages not equal.  \n"
		               << "******Patch:*****\n"
		               << std::endl;
		         std::cout << std::setw(4)<< mPatched << std::endl;

		}
		else{
			SUCCEED();
		}

	}


private:
	json mSource;
	json mTarget;
	json mPatch;
	json mPatched;
	bool yesDiff = false;
	std::vector <std::string> mIgPath;


	void setInit(json Source, json Target,
			std::vector <std::string> IgPath){

		mSource = Source;
		mTarget = Target;
		mPatch = json::diff(Source, Target);
		mIgPath = IgPath;
	}

	bool toIgnPath() { return !mIgPath.empty();}

	bool toIgnJson(json EmpJson) { return EmpJson.empty();}

	void fndIgPath(){

		//json delobj;

	    for(int i = 0; i < mPatch.size(); i++){

	    	json newobj = mPatch[i];
	    	std::string newpath = newobj["/path"_json_pointer];
	    	int dsexist = 0;

	    	if(toIgnPath()){
				for(int j = 0; j < mIgPath.size(); j++){
					int fnd = newpath.find(mIgPath[j]);
					//std::size_t fnd2 = newpath.find(findStr2);
					if (newpath.length() - fnd == mIgPath[j].length()) {
						dsexist = 1;
					}
				}
	    	}
	    	if(!dsexist){
	    		mPatched.push_back(mPatch[i]);
	    		yesDiff = true;
	    	}
	    }

	}


};
}
}
#endif
